package com.spring.requestheaders;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import controller.WebController;

@SpringBootApplication
@ComponentScan(basePackageClasses = WebController.class)
public class Exercitiul1RestApplication {

	public static void main(String[] args) {
		SpringApplication.run(Exercitiul1RestApplication.class, args);
	}
}
