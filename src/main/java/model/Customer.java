package model;

public class Customer {
	
	private String name;
	private Integer age;
	private Integer id;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	public Customer() {
		
	}
	
	public Customer(String name, Integer age, Integer id) {
		
		this.name = name;
		this.age = age;
		this.id = id;
	}
	
	@Override
	public String toString() {
		
		String info = String.format("Customer Info: name = %s, age = %d, id = %d",
                name, age, id);
		return info;
	}
	
	

}
